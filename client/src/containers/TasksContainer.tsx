import { Row, Col, Card, PageHeader } from 'antd'
import { message } from 'antd'
import { ITask } from '@redux/models/task.model'
import { useDispatch, useSelector } from 'react-redux'
import { addTask, removeTask, toggleTaskStatus } from '@redux/actions'
import CreateTask from '@components/CreateTask'
import { RootState } from '@redux/reducers'
import TaskList from '@components/TaskList'

const TasksContainer = () => {

  const dispatch = useDispatch()

  const { task } = useSelector((state: RootState) => state);

  const handleAddTask = (task: ITask): void => {
    dispatch(addTask(task))
    message.success('Đã thêm task!')
  };

  const handleRemoveTask = (task: ITask): void => {
    dispatch(removeTask(task));
    message.warn('Đã xóa task!');
  };

  const handleToggleTaskStatus = (task: ITask): void => {
    dispatch(toggleTaskStatus(task));
    message.info('Đã cập nhật trạng thái task!');
  };

  return (
    <Row
      justify="center"
      align="middle"
      gutter={[0, 20]}
      className="tasks-container"
    >
      <Col
        xs={{ span: 23 }}
        sm={{ span: 23 }}
        md={{ span: 21 }}
        lg={{ span: 20 }}
        xl={{ span: 18 }}
      >
        <PageHeader
          title="Add Task"
          subTitle="To add a task, just fill the form below and click in add task."
        />
      </Col>

      <Col
        xs={{ span: 23 }}
        sm={{ span: 23 }}
        md={{ span: 21 }}
        lg={{ span: 20 }}
        xl={{ span: 18 }}
      >
        <Card title="Tạo task mới">
          <CreateTask onFormSubmit={handleAddTask} />
        </Card>
      </Col>

      <Col
        xs={{ span: 23 }}
        sm={{ span: 23 }}
        md={{ span: 21 }}
        lg={{ span: 20 }}
        xl={{ span: 18 }}
      >
        <Card title="Danh sách task">
          <TaskList
            tasks={task.tasks}
            onTaskRemoval={handleRemoveTask}
            onTaskToggle={handleToggleTaskStatus}
          />
        </Card>
      </Col>
    </Row>
  )
}

export default TasksContainer
