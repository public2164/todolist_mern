export enum TASK_ACTION_TYPES {
  ADD_TASK_SUCCESS,
  TOGGLE_TASK_STATUS_SUCCESS,
  REMOVE_TASK_SUCCESS,
}
