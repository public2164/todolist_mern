import { TASK_ACTION_TYPES } from '@redux/constants'
import { ITask } from '@redux/models/task.model'
import { TaskActionTypes } from '@redux/actions/task.actions.types'
import { v1 as uuidV1 } from 'uuid'

interface ITaskReducerInterface {
  tasks: ITask[]
}

export const initialState: ITaskReducerInterface = {
  tasks: [],
}

export const taskReducer = (state = initialState, action: TaskActionTypes) => {
  switch (action?.type) {
    case TASK_ACTION_TYPES.ADD_TASK_SUCCESS:
      return Object.assign({}, state, {
        tasks: state.tasks.concat({
          ...action.payload,
          ...{
            id: action.payload.id ?? uuidV1(),
          },
        }),
      })
    case TASK_ACTION_TYPES.TOGGLE_TASK_STATUS_SUCCESS:
      return Object.assign({}, state, {
        tasks: state.tasks.map((task: ITask) =>
          task.id === action.payload.id
            ? { ...task, completed: !task.completed }
            : task
        ),
      })
    case TASK_ACTION_TYPES.REMOVE_TASK_SUCCESS:
      return {
        tasks: state.tasks.filter(
          (task: ITask) => task.id !== action.payload.id
        ),
      }
    default:
      return state
  }
}
