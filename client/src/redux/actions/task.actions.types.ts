import { TASK_ACTION_TYPES } from '@redux/constants'
import { ITask } from '@redux/models/task.model'

interface IAddTaskAction {
  type: typeof TASK_ACTION_TYPES.ADD_TASK_SUCCESS
  payload: ITask
}

interface IToggleTaskAction {
  type: typeof TASK_ACTION_TYPES.TOGGLE_TASK_STATUS_SUCCESS
  payload: ITask
}

interface IRemoveTaskAction {
  type: typeof TASK_ACTION_TYPES.REMOVE_TASK_SUCCESS
  payload: ITask
}

export type TaskActionTypes =
  | IAddTaskAction
  | IRemoveTaskAction
  | IToggleTaskAction

