import { ITask } from '@redux/models/task.model'
import { TaskActionTypes } from './task.actions.types'
import { TASK_ACTION_TYPES } from '../constants'

export const addTask = (task: ITask): TaskActionTypes => {
  return {
    type: TASK_ACTION_TYPES.ADD_TASK_SUCCESS,
    payload: task,
  }
}

export const removeTask = (task: ITask): TaskActionTypes => {
  return {
    type: TASK_ACTION_TYPES.REMOVE_TASK_SUCCESS,
    payload: task,
  }
}

export const toggleTaskStatus = (task: ITask): TaskActionTypes => {
  return {
    type: TASK_ACTION_TYPES.TOGGLE_TASK_STATUS_SUCCESS,
    payload: task,
  }
}
