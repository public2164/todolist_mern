import React from 'react'
import { ITask } from '@redux/models/task.model'
import { Button, Col, Form, Input, Row } from 'antd'
import { PlusCircleFilled } from '@ant-design/icons'

interface ICreateTaskFormProps {
  onFormSubmit: (task: ITask) => void
}

const CreateTask: React.FC<ICreateTaskFormProps> = ({ onFormSubmit }) => {
  const [form] = Form.useForm()

  const onFinish = () => {
    onFormSubmit({
      name: form.getFieldValue('name'),
    })

    form.resetFields()
  };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      layout="horizontal"
      className="task-form"
    >
      <Row gutter={20}>
        <Col xs={24} sm={24} md={17} lg={19} xl={20}>
          <Form.Item
            name={'name'}
            rules={[{ required: true, message: 'Bắt buộc phải nhập nội dung.' }]}
          >
            <Input placeholder="Những gì cần thực hiện?" />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={7} lg={5} xl={4}>
          <Button type="primary" htmlType="submit" block>
            <PlusCircleFilled />
            Thêm task
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

export default CreateTask
