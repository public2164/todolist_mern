import { Tooltip, Tag, List, Button, Popconfirm, Switch } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import { ITask } from '@redux/models/task.model'

interface ITaskItemProps {
  task: ITask
  onTaskRemoval: (task: ITask) => void
  onTaskToggle: (task: ITask) => void
}

const TaskItem:React.FC<ITaskItemProps> = ({
  task,
  onTaskRemoval,
  onTaskToggle,
}) => {
  return (
    <List.Item
      actions={[
        <Tooltip
          title={task.completed ? 'Mark as uncompleted' : 'Mark as completed'}
        >
          <Switch
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
            onChange={() => onTaskToggle(task)}
            defaultChecked={task.completed}
          />
        </Tooltip>,
        <Popconfirm
          title="Are you sure you want to delete?"
          onConfirm={() => {
            onTaskRemoval(task);
          }}
        >
          <Button className="remove-task-button" type="primary" danger>
            X
          </Button>
        </Popconfirm>,
      ]}
      className="list-item"
      key={task.id}
    >
      <div className="task-item">
        <Tag color={task.completed ? 'cyan' : 'red'} className="task-tag">
          {task.name}
        </Tag>
      </div>
    </List.Item>
  )
}

export default TaskItem
