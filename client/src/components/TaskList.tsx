import { ITask } from "@redux/models/task.model";
import { List } from "antd"
import TaskItem from "./TaskItem";

interface ITaskListProps {
  tasks: ITask[]
  onTaskRemoval: (task: ITask) => void
  onTaskToggle: (task: ITask) => void
}

const TaskList: React.FC<ITaskListProps> = ({
  tasks,
  onTaskRemoval,
  onTaskToggle,
}) => {
  return (
    <List
      locale={{
        emptyText: "There's nothing to do :(",
      }}
      dataSource={tasks}
      renderItem={(task) => (
        <TaskItem
          task={task}
          onTaskToggle={onTaskToggle}
          onTaskRemoval={onTaskRemoval}
        />
      )}
      pagination={{
        position: 'bottom',
        pageSize: 5,
      }}
    />
  )
}

export default TaskList
