import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import TasksContainer from '@containers/TasksContainer';

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<TasksContainer />} />
      </Routes>
    </Router>
  );
}

export default App
