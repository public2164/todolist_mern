import mongoose from 'mongoose'

const URI = process.env.MONGODB_URL

export const DBConnect = () => {
  mongoose.connect(`${URI}`, err => {
    if (err) throw err
    console.log('Connected to mongodb.')
  })
}
